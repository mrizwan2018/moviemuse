//
//  Movie.swift .swift
//  MovieMuse
//
//  Created by Zahid Shabbir on 15/05/2023.
//

import Foundation
import UIKit


import Foundation

// MARK: - MoviesResponseModel
struct MoviesResponseModel: Codable {
    let movies: [Movie]?
    let totalPages, totalResults, page: Int?

    enum CodingKeys: String, CodingKey {
        case movies = "results"
        case totalPages = "total_pages"
        case totalResults = "total_results"
        case page
    }
}

// MARK: - Result
struct Movie: Codable {
    let genreIDS: [Int]?
    let adult: Bool?
    let backdropPath: String?
    let id: Int?
    let originalTitle: String?
    let voteAverage, popularity: Double?
    let posterPath, overview, title, originalLanguage: String?
    let voteCount: Int?
    let releaseDate: String?
    let video: Bool?
    var isFavorited: Bool = false 

    enum CodingKeys: String, CodingKey {
        case genreIDS = "genre_ids"
        case adult
        case backdropPath = "backdrop_path"
        case id
        case originalTitle = "original_title"
        case voteAverage = "vote_average"
        case popularity
        case posterPath = "poster_path"
        case overview, title
        case originalLanguage = "original_language"
        case voteCount = "vote_count"
        case releaseDate = "release_date"
        case video
    }
}
