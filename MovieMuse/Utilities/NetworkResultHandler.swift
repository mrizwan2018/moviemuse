//
//  NetworkResultHandler.swift
//  MovieMuse
//
//  Created by Zahid Shabbir on 19/05/2023.
//

import Foundation

/// Utility struct for handling network request results.
struct NetworkResultHandler {
    /// Handles the result of a network request.
    ///
    /// - Parameters:
    ///   - result: The result of the network request, either containing a value of type `T` or an error.
    ///   - completion: A closure to be called with the value and error (if any) from the result.
    static func handleResult<T>(_ result: Result<T, Error>, completion: @escaping (T?, Error?) -> Void) {
        switch result {
        case .success(let value):
            completion(value, nil)
        case .failure(let error):
            completion(nil, error)
        }
    }
}
