//
//  Common.swift
//  MovieMuse
//
//  Created by Zahid Shabbir on 16/05/2023.
//

import Foundation
import UIKit

/// Typealias for a dictionary of parameters used in network requests.
typealias ParamsType = [String: Any]?
let imageBaseUrl = "https://image.tmdb.org/t/p/w92/"
/// Enum representing different HTTP methods.
enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
    // ... add more cases as needed
}

/// Extension on SceneDelegate to load a view controller from a nib.
extension SceneDelegate {
    /// Loads a view controller from a nib and sets it as the root view controller.
    ///
    /// - Parameter controller: The view controller to be loaded and set as the root view controller.
    public func loadNibWith(controller: UIViewController) {
        self.navController = UINavigationController(rootViewController: controller)
        self.navController?.navigationBar.barStyle = .default
        self.navController.navigationBar.shadowImage = UIImage.init()
        self.navController.navigationBar.setBackgroundImage(UIImage.init(), for: .default)
        self.navController.setNavigationBarHidden(true, animated: false)
        self.navController.navigationBar.tintColor = UIColor.systemGray
        self.navController.navigationBar.isHidden = true
        self.window?.rootViewController = self.navController
        self.window?.backgroundColor = .white
        self.window?.makeKeyAndVisible()
    }
}

extension UIViewController {
    /// A generic method to instantiate a view controller from a nib.
    ///
    /// - Returns: An instance of the view controller after initializing it from the nib.
    static func instantiate() -> Self {
        func instantiateFromNib<T: UIViewController>(_ viewType: T.Type) -> T {
            return T(nibName: String(describing: T.self), bundle: nil)
        }
        return instantiateFromNib(self)
    }
    
    /// Shows an alert with the given title and message.
    ///
    /// - Parameters:
    ///   - title: The title of the alert.
    ///   - message: The message of the alert.
    func showAlert(title: String = "Error", message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true)
    }
}

extension Data {
    /// Prints the data in a pretty JSON format.
    func printPretty() {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: self, options: .allowFragments)
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            print(String(data: jsonData, encoding: .utf8) ?? "Sorry cannot print Pretty :(")
        } catch _ {
            print("Sorry cannot printPretty :( it's not valid JSON data")
        }
    }
    
    /// Parses the data to a Codable type.
    ///
    /// - Parameters:
    ///   - completion: A closure to be called with the parsed result or an error message.
    func parseTo<T: Codable>(completion: @escaping (T?, String?) -> Void) {
        do {
            let result = try JSONDecoder().decode(T.self, from: self)
            DispatchQueue.main.async {
                completion(result, nil)
            }
        } catch let DecodingError.dataCorrupted(context) {
            
            let message = context.debugDescription
            print("not found:\n", message)
            completion(nil, message)
        } catch let DecodingError.keyNotFound(key, context) {
            var message = "Key \(key ) not found: \(context.debugDescription)"
            message.append("\n codingPath: \(context.codingPath)\n")
            print(message)
            completion(nil, message)
        } catch let DecodingError.valueNotFound(value, context) {
            var message = "Value \(value ) not found: \(context.debugDescription)"
            message.append("\n codingPath: \(context.codingPath)\n")
            print(message)
            completion(nil, message)
        } catch let DecodingError.typeMismatch(type, context) {
            var message = "Type \(type) mismatch: \(context.debugDescription)"
            message.append("\n codingPath: \(context.codingPath)\n")
            print(message)
            completion(nil, message)
        } catch {
            print("error: ", "\(error)")
            completion(nil, "codingPath: \(error.localizedDescription)")
        }
    }
}

extension URL {
    /// Asynchronously downloads data from the URL.
    ///
    /// - Parameter completion: A closure to be called with the downloaded data, response, and error.
    func asyncDownload(completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared
            .dataTask(with: self, completionHandler: completion)
            .resume()
    }
}

extension URLRequest {
    /// Asynchronously downloads data using the URLRequest.
    ///
    /// - Parameter completion: A closure to be called with the downloaded data, response, and error.
    func asyncDownload(completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared
            .dataTask(with: self, completionHandler: completion)
            .resume()
    }
}


extension UIImageView {
    func download(from url: URL, placeholder: String = "popcorn.circle.fill") {
        let imageCache = NSCache<NSString, UIImage>()
        let urlStringIs = url.absoluteString
        //print("downloading image url = \(urlStringIs)")
        if let imageFromCache = imageCache.object(forKey: urlStringIs as NSString) {
            self.image = imageFromCache
            return
        }
        self.image = UIImage(systemName: placeholder)
        //        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async { [weak self] in
                self?.image = image
                imageCache.setObject(image, forKey: urlStringIs as NSString)
            }
        }.resume()
    }
    func downloads(from link: String) {
        guard let url = URL(string: link) else {
            self.image =  UIImage(systemName: "popcorn")
            return
        }
        download(from: url, placeholder: "popcorn")
    }
    
    func changeImage(color: UIColor) {
        if let image = self.image {
            self.image = image.withRenderingMode(.alwaysTemplate)
            self.tintColor = color
        }
    }
}
extension UIImage {
    static let heartFillImage = UIImage(systemName: "heart.fill")?.withTintColor(.red, renderingMode: .alwaysOriginal)
    static let heartImage = UIImage(systemName: "heart")?.withTintColor(.lightGray, renderingMode: .alwaysOriginal)
}

extension UIView {
    
    func addGradient(colors: [UIColor], startPoint: CGPoint = CGPoint(x: 0.0, y: 0.0), endPoint: CGPoint = CGPoint(x: 1.0, y: 1.0)) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = colors.map { $0.cgColor }
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        layer.insertSublayer(gradientLayer, at: 0)
    }
    func addShadow(color: UIColor = .black,
                   opacity: Float = 0.2,
                   offset: CGSize = CGSize(width: 0, height: 0),
                   radius: CGFloat = 10.0,
                   spread: CGFloat = 0.0) {
        let shadowPath = UIBezierPath(rect: bounds.insetBy(dx: -spread, dy: -spread))
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = shadowPath.cgPath
        shadowLayer.fillColor = backgroundColor?.cgColor
        shadowLayer.shadowColor = color.cgColor
        shadowLayer.shadowOffset = offset
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = radius
        
        layer.insertSublayer(shadowLayer, at: 0)
    }
    
    func dropShadow(color: UIColor = .black) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        self.layer.cornerRadius = 6
        self.clipsToBounds = true
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale

    }
    
    func applyCornerRadius(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
}

protocol Injectable {
    associatedtype Dependency
    func inject(dependency: Dependency)
}

extension Injectable where Self: UIViewController {
    static func instantiate(with dependency: Dependency) -> Self {
        let viewController = Self(nibName: String(describing: Self.self), bundle: nil)
        viewController.inject(dependency: dependency)
        return viewController
    }
}


extension String {
    var isNotEmpty: Bool { return !self.isEmpty }
    var trim: String { return self.trimmingCharacters(in: .whitespacesAndNewlines)}
}
extension Bool {
    var isFalse: Bool { return !self }
}
