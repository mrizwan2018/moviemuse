//
//  Observable.swift .swift
//  MovieMuse
//
//  Created by Zahid Shabbir on 15/05/2023.
//

import Foundation
extension Optional {
    @discardableResult
    public func unwrap(_ closure: (Wrapped) -> Void) -> Optional {
        if let value = self {
            closure(value)
        }
        return self
    }
    
    public static func unwrap(_ optionals: Optional<Wrapped>..., closure: () -> Void) {
        if optionals.allSatisfy({ $0 != nil }) {
            closure()
        }
    }
}
