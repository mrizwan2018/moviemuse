//
//  MovieListViewController.swift
//  MovieMuse
//
//  Created by Zahid Shabbir on 15/05/2023.
//
import UIKit
import Combine

class MovieListViewController: UIViewController, UICollectionViewDelegate { //},
    
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet private var collectionView: UICollectionView!
    
    private var viewModel: MovieListViewModel!
    private var cancellables: Set<AnyCancellable> = []
    
    var isSearching = false {
        didSet {
            restoreCollectionViewLayout()
        }
    }
    private var originalContentOffset: CGPoint = .zero
    private var debounceTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupViewModel()
        bindViewModel()
        setupViews()
        viewModel.fetchPopularMovies()
        
        viewModel.$movies.sink(receiveValue: { [weak self] _ in
            self?.collectionView.reloadData()
        }).store(in: &cancellables)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    private func setupViewModel() {
        let movieService = APIMovieService()
        let router = MovieListRouter(viewController: self)
        viewModel = MovieListViewModel(movieService: movieService, router: router)
    }
    
    private func bindViewModel() {
        viewModel.$movies
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.collectionView.reloadData()
            }
            .store(in: &cancellables)
        
        viewModel.$error
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                guard let self = self, let error = error else { return }
                self.displayError(message: error.localizedDescription)
            }
            .store(in: &cancellables)
    }
    
    fileprivate func restoreCollectionViewLayout() {
        if isSearching {
            let searchLayout = UICollectionViewFlowLayout()
            searchLayout.scrollDirection = .vertical

            // Calculate the item width based on the available width and desired number of items per row
            let availableWidth = collectionView.bounds.width
            let numberOfItemsPerRow: CGFloat = 2
            let cellWidth = (availableWidth - (searchLayout.minimumInteritemSpacing * (numberOfItemsPerRow - 1))) / numberOfItemsPerRow

            // Set the item size in the layout
            searchLayout.itemSize = CGSize(width: cellWidth, height: 150) // Adjust the cellHeight to your desired value

            collectionView.collectionViewLayout = searchLayout
        }else{
            let layout = TiltedFlowLayout(itemSizeRatio: 0.90)
            collectionView.collectionViewLayout = layout
            collectionView.contentOffset = originalContentOffset
        }
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        let movieListCellNib = UINib(nibName: Constants.movieListCell, bundle: nil)
        collectionView.register(movieListCellNib, forCellWithReuseIdentifier: Constants.movieListCell)
        
        restoreCollectionViewLayout()
        
        
    }
    
    func displayError(message: String) {
        let alert = UIAlertController(title: Constants.errorTitle, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: Constants.okActionTitle, style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func updateMovieFavoriteStatus(at index: Int, isFavorited: Bool) {
        let indexPath = IndexPath(item: index, section: 0)
        if let cell = collectionView.cellForItem(at: indexPath) as? MovieListCell {
            let heartImage = isFavorited ? UIImage(systemName: "heart.fill"): UIImage(systemName: "heart")
            cell.heartButton.setImage(heartImage, for: .normal)
        }
    }
    
    private func setupViews() {
        setupSearchContainerView()
        setupCollectionView()
    }
    
    private func setupSearchContainerView() {
        searchContainerView.applyCornerRadius(5)
        searchContainerView.backgroundColor = #colorLiteral(red: 0.125, green: 0.1289999932, blue: 0.175999999, alpha: 1)
        searchTextField.layer.borderWidth = 0
        searchContainerView.layer.borderColor = #colorLiteral(red: 0.09803921569, green: 0.1019607843, blue: 0.1450980392, alpha: 1)
        searchContainerView.layer.borderWidth = 0.5
        configureClearButtonAppearance()
        searchTextField.delegate = self
    }
    
    private func configureClearButtonAppearance() {
        if let clearButton = searchTextField.value(forKey: "_clearButton") as? UIButton {
            clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
            clearButton.tintColor = .white // Set your desired color
        }
    }
    
}

extension MovieListViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.viewModel.showMovieDetail(for: self.viewModel.movies[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.movieListCell, for: indexPath) as? MovieListCell else {
            return UICollectionViewCell()
        }
        let movie = viewModel.movies[indexPath.item]
        cell.configure(with: movie) { [weak self] in
            let isFavorited = self?.viewModel.toggleFavorite(at: indexPath.row)
            self?.updateMovieFavoriteStatus(at: indexPath.row, isFavorited: isFavorited!)
        }
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let collectionView = scrollView as? UICollectionView else {return}
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        let availableWidth = view.bounds.width - layout.sectionInset.left - layout.sectionInset.right
        let cellWidth = (availableWidth - layout.minimumInteritemSpacing * (CGFloat(Constants.columnsCount) - 1)) / CGFloat(Constants.columnsCount)
        
        let offsetX = scrollView.contentOffset.x
        let contentWidth = scrollView.contentSize.width
        let width = scrollView.frame.size.width
        
        let cellWidthIncludingSpacing = cellWidth + layout.minimumInteritemSpacing
        let preloadOffset = cellWidthIncludingSpacing * 3 // preload when 2 cells are left
        
        print("🧲 \(offsetX) > \(contentWidth - width) = \(contentWidth - width - preloadOffset) 🧲")
        
        
        if offsetX > contentWidth - width - preloadOffset {
            if isSearching, let text = searchTextField.text, text.count > 0 {
                viewModel.searchMovies(with: searchTextField.text ?? "")
            }else{
                viewModel.fetchPopularMovies()
            }
        }
        
        
        
    }
    
    
}
extension MovieListViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString text: String) -> Bool {
        guard let currentText = textField.text, let textRange = Range(range, in: currentText) else {
            return true
        }
        
        let updatedText = currentText.replacingCharacters(in: textRange, with: text)
        
        // Invalidate the debounce timer
        debounceTimer?.invalidate()
        if text.trim.isEmpty {
            // Start a new debounce timer after a delay of 0.5 seconds
            debounceTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { [weak self] _ in
                // Perform the search with the updated text
                self?.handleSearch(updatedText)
            }
            
        }else{
            self.handleSearch(updatedText)
        }
        
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        // Handle search when editing is done
        if let text = textField.text {
            handleSearch(text)
        }
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        // Clear the search and restore original state
        textField.text = nil
        handleSearch("")
        return true
    }
    
    private func handleSearch(_ searchText: String) {
        print("you searched \(searchText)")
        
        // Store the original content offset before performing the search
        originalContentOffset = collectionView.contentOffset
        
        // Perform search API call
        if searchText.isEmpty {
            // Restore original state
            viewModel.movies = viewModel.originalMovies
            UIView.animate(withDuration: 0.3) {
                self.collectionView.contentOffset = self.originalContentOffset
            }
            isSearching = false
        } else {
            if isSearching.isFalse{
                viewModel.originalMovies = viewModel.movies
            }
            // Call the search function in the view model
            viewModel.searchMovies(with: searchText)
            self.isSearching = true
        }
        
        // Scroll back to the previous position
        
    }


}

struct Constants {
    static let movieListCell = "MovieListCell"
    static let columnsCount = 2
    static let contentHorizontalPadding: CGFloat = 5
    static let contentVerticalPadding: CGFloat = 5
    static let cellsHorizontalPadding: CGFloat = 10
    static let cellsVerticalPadding: CGFloat = 10
    static let errorTitle = "Error"
    static let okActionTitle = "OK"
}

struct Section: Hashable {
    let title: String
    let items: [Item]
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(title)
    }
    
    static func ==(lhs: Section, rhs: Section) -> Bool {
        return lhs.title == rhs.title
    }
}

struct Item: Hashable {
    
    let identifier: String
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    
    static func ==(lhs: Item, rhs: Item) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
