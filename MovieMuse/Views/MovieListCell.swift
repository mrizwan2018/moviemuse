//
//  MovieListCell.swift
//  MovieMuse
//
//  Created by Zahid Shabbir on 18/05/2023.
//

import UIKit

class MovieListCell: UICollectionViewCell {
    
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var heartButton: UIButton!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var releaseDateLabel: UILabel!
    var toggleFavorite: (() -> Void)?

   let ColorFrom =  #colorLiteral(red: 0.07800000161, green: 0.08200000226, blue: 0.125, alpha: 1)
    let colorTo = #colorLiteral(red: 0.125, green: 0.1289999932, blue: 0.175999999, alpha: 1)

    func configure(with movie: Movie, toggleFavorite: @escaping () -> Void) {
        
        // Configure the cell UI with movie data
        layer.cornerRadius = 5
//        movieNameLabel.backgroundColor = #colorLiteral(red: 0.125, green: 0.1289999932, blue: 0.175999999, alpha: 1)
//        releaseDateLabel.backgroundColor = #colorLiteral(red: 0.125, green: 0.1289999932, blue: 0.175999999, alpha: 1)
        movieNameLabel.text = movie.title
        releaseDateLabel.text = movie.releaseDate
        movieNameLabel.textAlignment = .center
        releaseDateLabel.textAlignment = .center
        let urlString = imageBaseUrl + (movie.posterPath ?? "")
        posterImageView.downloads(from: urlString)
        
        
        let heartImage = movie.isFavorited ? UIImage(systemName: "heart.fill"): UIImage(systemName: "heart")
        heartButton.setImage(heartImage, for: .normal)
        heartButton.addTarget(self, action: #selector(heartButtonTapped), for: .touchUpInside)
        
        self.toggleFavorite = toggleFavorite
    }
    

    @IBAction func heartButtonTapped(_ sender: UIButton) {
        toggleFavorite?()
    }
}

